class UniqueIdValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    if value.present? && value.match(/[\s|ä|ö|ü|ß|Ä|Ö|Ü]/)
      record.errors[attribute] << I18n.t(:unique_id_contains_invalid_characters)
    end
  end
end
