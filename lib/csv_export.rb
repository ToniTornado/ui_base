require 'csv'
include ActionView::Helpers::NumberHelper

class CsvExport

  def self.csv_line(table_name, csv_object, attributes, translated_attributes)
    str = ""
    csv8 = []
    attributes.each do |each_attribute|
      csv_element = ''
      if each_attribute.present?
        index = attributes.find_index(each_attribute)
        display_attribute =  csv_object[index].is_a?(BigDecimal) ? csv_object[index] : csv_object[index].to_s
        use_phony_format = each_attribute.to_s.ends_with?('_phony')
        each_attribute = use_phony_format ? each_attribute.to_s.slice(0..-7).to_sym : each_attribute
        use_date_format = each_attribute.to_s.ends_with?('_as_date')
        each_attribute = use_date_format ? each_attribute.to_s.slice(0..-9).to_sym : each_attribute
        use_time_format = each_attribute.to_s.ends_with?('_as_time')
        each_attribute = use_time_format ? each_attribute.to_s.slice(0..-9).to_sym : each_attribute
        force_string_format = each_attribute.to_s.ends_with?('_force_string')
        each_attribute = force_string_format ? each_attribute.to_s.slice(0..-14).to_sym : each_attribute
        csv_element = display_attribute
        csv_element = I18n.t("#{table_name}.#{display_attribute}") if translated_attributes.include?(each_attribute) && display_attribute.present?
        if use_phony_format
          begin
            csv_element = Phony.format(Phony.normalize(display_attribute))
          rescue Phony::NormalizationError
            csv_element = display_attribute
          end
        end
        if use_date_format
          begin
            csv_element = Date.parse(display_attribute).iso8601
          rescue => error
            csv_element = display_attribute
          end
        end
        if use_time_format
          begin
            csv_element = I18n.l(Time.parse(display_attribute), format: '%Y-%m-%d %H:%M:%S')
          rescue => error
            csv_element = display_attribute
          end
        end
        if force_string_format
          csv_element = "'#{display_attribute}'"
        end
        csv8 << csv_element
      else
        csv8 << ''
      end
    end
    csv_enc = csv8.collect do |each|
      if each.blank?
        ''
      else
        if each.is_a?(BigDecimal)
          number_with_precision(each, separator: I18n.t("number.format.separator"), delimiter: nil, precision: 2 )
        else
          if each.is_a?(Date) || each.is_a?(Time) || each == true || each == false
            each
          else
            if each.is_a?(String)
              str = each
            else
              str = each.label
            end
            begin
              str.encode(Settings.csv.export_encoding)
            rescue Encoding::UndefinedConversionError, Encoding::CompatibilityError
              index = attributes.find_index('id')
              Rails.logger.warn "csv export encoding error in #{table_name}/#{csv_object[index]} value #{str}"
              I18n.t(:value_with_wrong_encoding_in_db)
            end
          end
        end
      end
    end
    csv_enc
  end

  def self.csv_line_from_objects(csv_object, attributes, translated_attributes)
    str = ""
    csv8 = []
    attributes.each do |each_attribute|
      if each_attribute.present?
        use_phony_format = each_attribute.to_s.ends_with?('_phony')
        each_attribute = use_phony_format ? each_attribute.to_s.slice(0..-7).to_sym : each_attribute
        use_date_format = each_attribute.to_s.ends_with?('_as_date')
        each_attribute = use_date_format ? each_attribute.to_s.slice(0..-9).to_sym : each_attribute
        use_time_format = each_attribute.to_s.ends_with?('_as_time')
        each_attribute = use_time_format ? each_attribute.to_s.slice(0..-9).to_sym : each_attribute
        force_string_format = each_attribute.to_s.ends_with?('_force_string')
        each_attribute = force_string_format ? each_attribute.to_s.slice(0..-14).to_sym : each_attribute
        csv_element = csv_object.send(each_attribute).to_s
        if translated_attributes.include?(each_attribute) && display_attribute.present?
          csv_element = I18n.t("#{csv_object.class.table_name}.#{csv_element}")
        end
        if use_phony_format
          begin
            csv_element = Phony.format(Phony.normalize(csv_element))
          rescue Phony::NormalizationError
            csv_element = csv_element
          end
        end
        if use_date_format
          begin
            csv_element = Date.parse(csv_element).iso8601
          rescue => error
            csv_element = csv_element
          end
        end
        if use_time_format
          begin
            csv_element = I18n.l(Time.parse(csv_element), format: '%Y-%m-%d %H:%M:%S')
          rescue => error
            csv_element = csv_element
          end
        end
        if force_string_format
          csv_element = "'#{display_attribute}'"
        end
      end
      csv8 << csv_element
    end
    csv_enc = csv8.collect do |each|
      if each.blank?
        ''
      else
        if each.is_a?(Numeric)
          number_with_precision(each, :separator => I18n.t("number.format.separator"), :delimiter => nil)
        else
          # TODO: Warum wird bei Timestamps hier nicht reingegangen?
          if each.is_a?(Date) || each.is_a?(Time) || each == true || each == false
            each
          else
            if each.is_a?(String)
              str = each
            else
              str = each.label
            end
            begin
              str.encode(Settings.csv.export_encoding)
            rescue Encoding::UndefinedConversionError, Encoding::CompatibilityError
              Rails.logger.warn "csv export encoding error in #{csv_object.class.name}/#{csv_object.id.to_s} value #{str}"
              I18n.t(:value_with_wrong_encoding_in_db)
            end
          end
        end
      end
    end
    csv_enc
  end


  def self.headline_part(object_class, attributes)
    csv8 = attributes.collect do |each|
      each = each.to_s.ends_with?('_phony') ? each.to_s.slice(0..-7).to_sym : each
      each = each.to_s.ends_with?('_as_date') ? each.to_s.slice(0..-9).to_sym : each
      each = each.to_s.ends_with?('_as_time') ? each.to_s.slice(0..-9).to_sym : each
      each = each.to_s.ends_with?('_force_string') ? each.to_s.slice(0..-14).to_sym : each
      if each.present?
        if (each =~ / /).present?
          each = each.split(' ').last
        end
        object_class.human_attribute_name(each.to_s.gsub(/[.\s]/, '_'))
      else
        ''
      end
    end
    csv8
  end

  def self.csv_headline(object_class_1, attributes_1, object_class_2 = nil, attributes_2 = [])
    csv8 = headline_part(object_class_1, attributes_1)
    csv8 << headline_part(object_class_2, attributes_2 - [nil]) if object_class_2.present?
    csv_enc = csv8.flatten.collect do |each|
      each.encode(Settings.csv.export_encoding)
    end
    csv_enc
  end

  def self.csv_data_for(collection, attributes, translated_attributes=[])
    if collection.any?
      data = CSV.generate(col_sep: Settings.csv.separator) do |csv|
        csv << CsvExport.csv_headline(collection.first.class, attributes)
        table_name = collection.first.class.table_name
        db_attributes = attributes
        db_attributes << 'id' unless db_attributes.include? 'id'
        db_attributes = db_attributes.collect do |each|
          each = each.to_s.ends_with?('_phony') ? each.to_s.slice(0..-7).to_sym : each
          each = each.to_s.ends_with?('_as_date') ? each.to_s.slice(0..-9).to_sym : each
          each = each.to_s.ends_with?('_as_time') ? each.to_s.slice(0..-9).to_sym : each
          each = each.to_s.ends_with?('_force_string') ? each.to_s.slice(0..-14).to_sym : each
        end
        collection.pluck_in_batches(db_attributes, batch_size: 1000) do |batch|
          batch.each do |object|
            csv << CsvExport.csv_line(table_name, object, attributes, translated_attributes)
          end
          GC.start
        end
        GC.start
      end
    end
    data
  end

  def self.send_csv_response(controller, csv_data, object_class, filename = nil)
    timestamp = Time.now.strftime("%Y%m%d-%H%M%S")
    object_class_name = object_class.model_name.human(count: 2)
    filename = "#{timestamp}-#{object_class_name.downcase}-export.csv" unless filename
    controller.send_data(csv_data,
      type: 'text/csv; ' + Settings.csv.export_encoding  + '; header=present',
      disposition: "attachment; filename=#{filename}"
    )
  end

  def self.send_csv(controller, collection, attributes, translated_attributes=[], filename = nil)
    if collection.empty?
      controller.redirect_to :back, notice: I18n.t('csv.no_records_found')
      return
    end
    object_class = collection.first.class
    csv_data = csv_data_for(collection, attributes, translated_attributes)
    send_csv_response(controller, csv_data, object_class, filename)
  end

end
