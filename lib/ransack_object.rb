module RansackObject

  def self.included(base)
    # Nur für Oracle einbinden; denn Postgres hat Probleme MIT und Oracle hat
    # Probleme OHNE diese Modifikation - Hurra.
    # Ziel ist case-insensitive search, postgres benutzt automatisch ILIKE,
    # Oracle muss lower(column) ausführen. Letzteres verursacht Probleme mit
    # einem notwendigen DISTINCT auf eine Ransack-Suche...
    if defined?(ActiveRecord::ConnectionAdapters::OracleEnhancedAdapter)
      begin
        if ActiveRecord::Base.connection.instance_of?(ActiveRecord::ConnectionAdapters::OracleEnhancedAdapter)
          base.columns.each do |column|
            if column.type == :string
              base.ransacker column.name.to_sym, type: :string do
                Arel.sql("lower(#{base.table_name}.#{column.name})")
              end
            end
          end
        end
      rescue OCIError => e
        # during precompiling when building the docker image there is no available database
      end
    end
  end

end
