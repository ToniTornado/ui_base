class CurrencyValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    integer_places = options[:precision]-options[:scale]
    
    if value and (value > ((10 ** integer_places) - 1))
      record.errors[attribute] << I18n.t(:enter_correct_amount, :integer_places => integer_places.to_s)
    end

  end
end