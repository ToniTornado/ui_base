class PercentValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    max = options[:max]
    if !max
      max = 100
    end
    if !value
      return
    end
    if value > 100
      record.errors[attribute] << I18n.t(:max_percentage_exceeded, :max => max)
    end
  end
end