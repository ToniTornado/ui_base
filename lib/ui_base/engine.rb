module UiBase
  class Engine < ::Rails::Engine
    isolate_namespace UiBase

    config.before_initialize do
      config.i18n.load_path += Dir["#{config.root}/config/locales/**/*.yml"]
    end

    require 'haml-rails'
    require 'uglifier'
    require 'jquery-rails'
    require 'sass'
    require 'sass-rails'
    require 'bootstrap-sass'
    require 'bootswatch-rails'
    require 'font-awesome-sass'
    require 'kaminari'
    require 'bootstrap-kaminari-views'
    require 'kaminari-i18n'
    require 'config'
    require 'net/smtp'
    require 'ransack_object'
    require 'strip_attributes'
    require 'turbolinks'
    # TODO: this requires simpled_form_extended to be loaded BEFORE ui_base...!
    if Kernel.const_defined?('::MinimalFormBuilder')
      ENV['RANSACK_FORM_BUILDER'] = '::MinimalFormBuilder'
    elsif Kernel.const_defined?('::SimpleForm::FormBuilder')
      ENV['RANSACK_FORM_BUILDER'] = '::SimpleForm::FormBuilder'
    end
    require 'ransack' if ("ActiveRecord".constantize rescue nil).present?
    require 'phony'
    require 'csv_export'

  end
end
