module UiBase
  module LayoutHelper

    def title(title)
      @title = title
    end

    def page_header(a_string)
      @page_header = a_string
    end

  end
end
