module UiBase
  module ApplicationHelper

    def user_is_admin?
      current_user.user_type == 'admin'
    end

    def tel_to(text)
      groups = text.to_s.scan(/(?:^\+)?\d+/)
      link_to text, 'tel:' + groups.join('-')
    end

    def button_link_to(*args, &block)
      options = args.extract_options!

      button_type = options.delete :type
      button_size = options.delete :size
      button_icon = options.delete :icon
      button_icon_position = options.delete :icon_position
      css = "btn btn-default #{options[:class]}"
      css << " btn-#{button_type.to_s.dasherize}" if button_type.present?
      css << " btn-#{button_size}" if button_size.present?
      options[:class] = css.strip

      if block_given?
        route = args.first
        label = capture(&block)
      else
        label = args.first
        route = args.second
      end

      if button_icon.present?
        icon_tag = content_tag(:i, nil, class: "fa fa-fw fa-#{button_icon.to_s.dasherize}")
        if button_icon_position == :right
          label = h(label) + ' ' + icon_tag
        else
          label = icon_tag + ' ' + h(label)
        end
      end
      if options[:disabled]
        capture_haml do
          haml_tag 'span', class: options[:class] + ' disabled' do
            concat label
          end
        end
      else
        link_to(label.html_safe, route, options) + ' '
      end
    end
  end
end
