module UiBase
  module InfoHelper


    def used_engines
      specs = Bundler.environment.specs.to_hash
      gems = {}
      specs.each do | k,v |
        gems[k] = v.first.version.version if ['ui_base','job_management','simple_form_extended','ui_base', 'perfect_history'].include?(k) || k =~ /authentication/
      end
      gems
    end

    def hostname
      if !ENV["HOSTNAME"]
        if !ENV["COMPUTERNAME"]
          return `hostname`
        end
        return ENV["COMPUTERNAME"]
      end
      ENV["HOSTNAME"]
    end

    def nls_lang
      return ENV["NLS_LANG"] if ENV["NLS_LANG"]
      nil
    end

  end
end
