class TestMailer < ActionMailer::Base

  def test_email(email)
    mail(
      from: Settings.mail_sender,
      to: email,
      subject: I18n.t(:test_mail_subject)
    )
  end

end
