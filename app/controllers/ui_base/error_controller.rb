module UiBase
  class ErrorController < UiBase::ApplicationController

    skip_before_action :store_and_set_search_params, :store_and_set_per_page, :store_back_url

    def index
    end
    
  end
end
