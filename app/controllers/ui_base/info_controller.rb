module UiBase
  class InfoController < UiBase::ApplicationController

    skip_before_action :store_and_set_search_params, :store_and_set_per_page, :store_back_url, :init_search_params, only: [:check_availability]

    def index
    end

    def check_availability
      render plain: 'Alive and kicking!'
    end

    def send_test_email_to_current_user
      email = current_user.email
      begin
        TestMailer.test_email(email).deliver!
      rescue *SMTP_ERRORS, Errno::ECONNREFUSED => error
        redirect_to info_path, alert: (I18n.t('info.send_email_failed') + "<br/><br/>#{error.message}").html_safe
        return
      end
      redirect_to info_path, notice: I18n.t('info.email_test_success')
    end

    def reset_all_views
      session[:q] = {}
      redirect_to info_path, notice: I18n.t('info.reset_all_views_success')
    end

  end
end
