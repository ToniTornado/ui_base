module UiBase
  class ApplicationController < ActionController::Base

    protect_from_forgery with: :reset_session, prepend: true

    layout 'layouts/application'

    helper UiBase::LayoutHelper
    helper Rails.application.helpers

    before_action :init_search_params
    before_action :store_and_set_search_params, :store_and_set_per_page, only: [:index, :search]
    before_action :store_back_url

    def set_locale
      begin
        locale = params[:locale] || session[:locale]
        I18n.locale = locale || I18n.default_locale
        begin
          session[:locale] = I18n.locale
        rescue
          Rails.logger.warn "Set locale to session failed!"
        end
      rescue I18n::InvalidLocale
        I18n.locale = I18n.default_locale
      end
    end

    def store_back_url
      return if controller_name == 'sessions'
      return unless request.format == :html
      return unless ['index', 'show'].include? action_name
      session[:back_url] = request.original_url.split('?').first rescue :back
    end

    def back_url
      if session[:back_url] == request.original_url.split('?').first
        :back
      else
        session[:back_url]
      end
    end

    def init_search_params
      params[:q] ||= {}
    end

    def reset_session_search_params
      (session[:q][controller_name]['index'] = nil) rescue nil
      (session[:q][controller_name]['search'] = nil) rescue nil
      redirect_to action: 'index'
    end

    def search
      index
      render :index
    end

    def store_and_set_search_params
      # Equalize /search and /index and /
      action = action_name == 'search' ? 'index' : action_name
      # Store
      unless params[:q].empty?
        # Oracle kennt leider keine case insensitives LIKE -.-
        if ActiveRecord::Base.configurations[Rails.env]['adapter'] =~ /oracle/
          params[:q].each do |key, value|
            # this enables case insensitive search with _cont suffix
            # this fix requires lowercase on the database content also
            # see extension on RansackObject in lib
            if key.to_s.match(/_cont/)
              params[:q][key] = value.downcase
            end
          end
        end
        session[:q] = (session[:q] || {}).deep_merge({controller_name => { action => params[:q] }})
      end
      # Load
      @search_params = session[:q][controller_name][action] rescue {}
      @search_params_present = @search_params.present? && @search_params_present.nil?
    end

    def set_search_param_default(name, value)
      if @search_params_present == false
        name = name.to_sym
        params[:q][name] = value if params[:q][name].nil?
        store_and_set_search_params
      end
    end

    def store_and_set_per_page
      session[:per_page] = {} if session[:per_page].blank?
      # Store
      if params[:per_page]
        session[:per_page] = session[:per_page].deep_merge({controller_name => { action_name => params[:per_page].to_i }})
        # Falls plötzlich 50 Einträge pro Seite angezeigt werden, befindet sicher der Anwender ggf.
        # auf einer hohen Seitenzahl, die keine Einträge mehr hat! Lösung:
        redirect_to page: 1
        return
      end
      # Load
      @per_page = session[:per_page][controller_name][action_name].to_i rescue 10
      @per_page = 10 if @per_page > 100 || @per_page < 1
    end

  end
end
