$(function() {
  var placeholder = $('#form-buttons-placeholder')
  if (placeholder.length > 0) {
    stickyThreshold = placeholder.offset().top - 50
    $(window).scroll(function() {
      if ($(this).scrollTop() > stickyThreshold) {
        return $('#form-buttons').addClass('sticky')
      } else {
        return $('#form-buttons').removeClass('sticky')
      }
    })
  }
})
