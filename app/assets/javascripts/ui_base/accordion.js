// Toggle chevron icon
$(function() {
  $('.bs-accordion').on('hide.bs.collapse show.bs.collapse', function(n) {
    $(n.target).siblings('.panel-heading').find('i.fa').toggleClass('fa-rotate-90');
  })
})
