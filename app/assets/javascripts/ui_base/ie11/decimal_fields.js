$(document).ready(function() {

      var isIE11 = !!window.MSInputMethodContext && !!document.documentMode
      if (isIE11) {
        numeral.register('locale', 'de', {
            delimiters: {
                thousands: '.',
                decimal: ','
            },
            abbreviations: {
                thousand: 'k',
                million: 'm',
                billion: 'b',
                trillion: 't'
            },
            ordinal : function (number) {
                return number + '.'
            },
            currency: {
                symbol: '€'
            }
        });

        var current_locale = $('html').attr('lang')

        $('input.decimal').each(function(index, el) {
          numeral.locale('en');
          var decimal = numeral($(el).val())
          numeral.locale(current_locale);
          var formatted_decimal = decimal.format('0.00')
          $(el).val(formatted_decimal)
        })

        numeral.locale(current_locale);
        $('form').on("submit", function(e){
          e.preventDefault()
          $('input.decimal').each(function(index, el) {
            var decimal = numeral($(el).val())
            $(el).val(decimal.value())
          })
          e.target.submit()
       })
     }

})
