$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ui_base/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ui_base"
  s.version     = UiBase::VERSION
  s.authors     = ["Robin Wielpuetz", "Christian Noack"]
  s.email       = ["robin.wielpuetz@gmail.com", "christian.noack@agile-methoden.de"]
  s.homepage    = "http://www.agile-methoden.de"
  s.summary     = "Base UI Functionality."
  s.description = "Base UI Functionality."

  s.files = Dir["{app,config,db,lib}/**/*", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency 'rails', '~> 5.2'
  s.add_dependency 'turbolinks'
  s.add_dependency 'haml-rails'
  s.add_dependency 'coffee-rails'
  s.add_dependency 'uglifier'
  s.add_dependency 'jquery-rails'
  s.add_dependency 'sass'
  s.add_dependency 'sass-rails'
  s.add_dependency 'bootstrap-sass'
  s.add_dependency 'bootswatch-rails'
  s.add_dependency 'font-awesome-sass'
  s.add_dependency 'kaminari'
  s.add_dependency 'bootstrap-kaminari-views'
  s.add_dependency 'kaminari-i18n'
  s.add_dependency 'ransack'
  s.add_dependency 'config' # rails g config:install
  s.add_dependency 'strip_attributes'
  s.add_dependency 'phony' # needed for csv_export
end
