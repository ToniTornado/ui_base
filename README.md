# UiBase Gem

This project contains the ruby gem **ui_base**. This gem can be used
by Ruby-on-Rails applications which want to provide responsive design with bootstrap.

# Installation

## Prerequisist
Your application should be based on Ruby-on-Rails > 5.0.0. You should use [SASS](http://sass-lang.com/) in your project. If haven't yet add this line to your Gemfile:
```
gem 'sass-rails'
```

## Install Gem

* Change the source in your ``Gemfile`` to a repository containing this gem.
We suggest to replace the source line by this block of code:
```
if ENV['GEMREPO']
    source ENV['GEMREPO']
else
    source 'http://mynexus.example.com/nexus/content/repositories/rubygems'
end
```
You could then overwrite your source repository with your own by setting the enviroment
variable ``GEMREPO`` if you have a special setup in your development enviroment.

* Add this Gem to your Gemfile: ``gem 'ui_base'``
* run `bundle install`

## Prepare your application

* ``rails g config:install``
* Add the gem's route and the configartion for pagination and search dialog on the
index pages to ``config/routes.rb``:

```
mount UiBase::Engine => "/"

# Kaminari Pagination
concern :paginatable do
  get '(page/:page)', action: :index, on: :collection, as: ''
end

concern :searchable do
  get 'search/reset', action: :reset_session_search_params, on: :collection
  # Erlaube das Absenden des Suchformulars mittels Post,
  # da es ab ca. 2000 Zeichen in der URL knallt..
  post 'search', action: 'index', as: :search, on: :collection
end
```
* modify your ``routes.rb`` for all models that should be paginatable and searchable
to look like this: ``resources :my_class_name_in_plural, concerns: [:paginatable, :searchable]``
* Add this gems lib path to your applications load path, therefore add the following line
to your ``config/application.rb``:
```
config.autoload_paths += %W(#{UiBase::Engine.root}/lib)
config.autoload_paths += %W(#{UiBase::Engine.root}/app/inputs)
```
* Add the javascript assets to your ``app/asssets/javascript/application.js``
```
//= require ui_base/application
```
* Add the css assets to your ``app/asssets/javascript/application.scss``
```
@import "bootstrap-sprockets";
@import "bootstrap";
@import "ui_base/bootstrap-extensions";
@import "ui_base/application";
```
* remove your ``app/views/layouts/application.html.*``, because this is include in UIBase
* add ``helper UiBase::Engine.helpers`` to ApplicationController

## More features

* To configure the footer for your application, add this config to your ``config/settingy.yml``:

```
footer:
  visible: true # false to hide footer
  content: Your Company Name <a href="/imprint">Imprint</a> # automatically prefixed (c) YEAR
  alignment: center # left | center | right
```
