UiBase::Engine.routes.draw do
  get 'info' => 'info#index', as: :info
  get 'info/send_test_email_to_current_user', as: :test_email
  get 'info/reset_all_views', as: :reset_all_views
  get 'error' => 'error#index', as: :error
  get 'availability/check' => 'info#check_availability'
end
