String.class_eval do

  def fill_up_to_8_characters
    self.fixed_length(8)
  end

  def fixed_length(number_of_characters)
    (self + ' ' * number_of_characters)[0,number_of_characters]
  end

  def in_configured_format
    return upcase if Settings.disable_legacy_handling
    fixed_length(8).upcase
  end

end
