if defined? ActiveRecord::Relation  

  ActiveRecord::Relation.class_eval do

    def pluck_in_batches(columns, batch_size: 1000)
      if columns.empty?
       raise "There must be at least one column to pluck"
      end

      batch_start = 1
      select_columns = columns.dup
      remove_id_from_results = false
      id_index = columns.index(primary_key.to_sym)

      if id_index.nil?
       id_index = 0
       select_columns.unshift(primary_key)
       remove_id_from_results = true
      end

      loop do
       items = self.where(table[primary_key].gteq(batch_start)).limit(batch_size).order(table[primary_key].asc).pluck(*select_columns)
       break if items.empty?

       last_item = items.last
       last_id = last_item.is_a?(Array) ? last_item[id_index] : last_item

       items.map! {|row| row[1..-1]} if remove_id_from_results

       yield items

       break if items.size < batch_size
       batch_start = last_id + 1
      end
    end
  end
end
